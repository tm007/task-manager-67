package ru.tsc.apozdnov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.apozdnov.tm.api.ProjectEndpoint;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.Arrays;
import java.util.List;

public class ProjectEndpointClient implements ProjectEndpoint {

    @NotNull
    private static final String URL = "http://localhost:8080/api/project/";

    @Override
    public List<Project> findAll() {
        @NotNull final String localUrl = "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        return Arrays.asList(template.getForObject(URL + localUrl, Project[].class));
    }
    @Override
    public Project findById(@NotNull final String id) {
        @NotNull final String localUrl = "findById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(URL + localUrl, Project.class, id);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @NotNull final String localUrl = "existsById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(URL + localUrl, Boolean.class, id);
    }

    @Override
    public Project save(@NotNull final Project project) {
        @NotNull final String localUrl = "save";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(project, headers);
        return template.postForObject(URL + localUrl, entity, Project.class);

    }

    @Override
    public void delete(@NotNull final Project project) {
        @NotNull final String localUrl = "delete";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(project, headers);
        template.postForObject(URL + localUrl, entity, Project.class);
    }

    @Override
    public void deleteAll(@NotNull final List<Project> projects) {
        @NotNull final String localUrl = "deleteAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<List<Project>> entity = new HttpEntity<>(projects, headers);
        template.postForObject(URL + localUrl, entity, Project[].class);
    }

    @Override
    public void clear() {
        @NotNull final String localUrl = "clear";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(URL + localUrl);
    }

    @Override
    public void deleteById(@NotNull final String id) {
        @NotNull final String localUrl = "deleteById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(URL + localUrl, id);
    }

    @Override
    public long count() {
        @NotNull final String localUrl = "count";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(URL + localUrl, Long.class);
    }

}
